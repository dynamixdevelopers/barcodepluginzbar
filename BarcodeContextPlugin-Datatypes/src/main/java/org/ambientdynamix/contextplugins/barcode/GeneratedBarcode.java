package org.ambientdynamix.contextplugins.barcode;

import android.os.Parcel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by maxpagel on 16/02/16.
 */
public class GeneratedBarcode implements IGeneratedBarcode {

    public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.barcode.generatebarcode";
    /**
     * Static Creator factory for Parcelable.
     */
    public static final Creator<GeneratedBarcode> CREATOR = new Creator<GeneratedBarcode>() {
        public GeneratedBarcode createFromParcel(Parcel in) {
            return new GeneratedBarcode(in);
        }

        public GeneratedBarcode[] newArray(int size) {
            return new GeneratedBarcode[size];
        }
    };

    byte[] barcode;

    public GeneratedBarcode(Parcel in) {
        barcode = new byte[in.readInt()];
        in.readByteArray(barcode);
    }

    public GeneratedBarcode(byte[] bytes) {
        barcode = bytes;
    }

    @Override
    public byte[] getBarcode() {
        return barcode;
    }

    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    @Override
    public String getImplementingClassname() {
        return ((Object)this).getClass().getName();
    }

    @Override
    public Set<String> getStringRepresentationFormats() {

        Set<String> formats = new HashSet<String>();
        return formats;
    }

    @Override
    public String getStringRepresentation(String s) {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(barcode.length);
        parcel.writeByteArray(barcode);
    }
}
