package org.ambientdynamix.contextplugins.barcode;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Created by maxpagel on 16/02/16.
 */
public interface IGeneratedBarcode extends IContextInfo{

    byte[] getBarcode();

}
