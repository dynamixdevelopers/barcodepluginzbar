/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.barcode;

import org.ambientdynamix.api.application.IContextInfo;

public interface IBarcodeContextInfo extends IContextInfo {
	/**
	 * Returns the barcode format as a string. Supported types include: QR_CODE, DATAMATRIX, UPC_E, UPC_A, EAN_8,
	 * EAN_13, CODE_128, CODE_39, ITF, RSS14 and PDF417.
	 */
	public abstract String getBarcodeFormat();

	/**
	 * Returns the barcode's value as a string.
	 */
	public abstract String getBarcodeValue();
}