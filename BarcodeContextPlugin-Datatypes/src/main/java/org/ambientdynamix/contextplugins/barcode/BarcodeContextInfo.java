/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.barcode;

import android.os.Parcel;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a barcode format and value.
 * 
 * @author Darren Carlson
 */
public class BarcodeContextInfo implements IBarcodeContextInfo {

    public static String BARCODE_SCAN_CONTEXT_TYPE = "org.ambientdynamix.contextplugins.barcode.scan";
	/**
	 * Static Creator factory for Parcelable.
	 */
	public static final Creator<BarcodeContextInfo> CREATOR = new Creator<BarcodeContextInfo>() {
		public BarcodeContextInfo createFromParcel(Parcel in) {
			return new BarcodeContextInfo(in);
		}

		public BarcodeContextInfo[] newArray(int size) {
			return new BarcodeContextInfo[size];
		}
	};
	// Private data
	private String format;
	private String value;

	/**
	 * Creates a BarcodeContextInfo.
	 */
	public BarcodeContextInfo(String format, String value) {
		this.format = format;
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.contextplugins.barcode.IBarcodeContextInfo#getBarcodeFormat()
	 */
	public String getBarcodeFormat() {
		return format;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.contextplugins.barcode.IBarcodeContextInfo#getBarcodeValue()
	 */
	public String getBarcodeValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return ((Object)this).getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		return format + ":" + value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return BARCODE_SCAN_CONTEXT_TYPE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	};

	@Override
	public String toString() {
		return ((Object)this).getClass().getSimpleName();
	}

	/**
	 * Private constructor for Parcelable.
	 */
	private BarcodeContextInfo(Parcel in) {
		format = in.readString();
		value = in.readString();
	}

	/**
	 * Used internally for Parcelable.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(format);
		out.writeString(value);
	}

	/**
	 * Used internally for Parcelable.
	 */
	public int describeContents() {
		return 0;
	}
}