/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.barcode;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.io.ByteArrayOutputStream;
import java.util.EnumMap;
import java.util.Map;
import java.util.UUID;

public class BarcodeContextPluginRuntime extends ContextPluginRuntime {
    private final String TAG = this.getClass().getSimpleName();
    public static String SCAN_TO_INTERACT_FEATURE = "org.ambientdynamix.contextplugins.barcode.features.scantointeract";

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        Log.d(TAG, "Setting PowerScheme " + scheme);
    }

    @Override
    public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(scheme);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // TODO Auto-generated method stub
    }

    @Override
    public void start() {
        Log.d(TAG, "Started!");
    }

    @Override
    public void stop() {
        Log.d(TAG, "Stopped!");
    }

    @Override
    public void destroy() {
        stop();
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        if (contextType.equals(BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE)) {
            openView(new CameraTestActivity(getSecuredContext(), this, requestId,
                    contextType));
        }
        else {
            sendContextRequestError(requestId, "Context Type not supported", ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
        }
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle requestConfig) {

        if(contextInfoType.equals(GeneratedBarcode.CONTEXT_TYPE)){

            String text = requestConfig.getString("BARCODE_TEXT"); // this is the text that we want to encode

            int dimension = 400;


            String imageFormat = "png"; // could be "gif", "tiff", "jpeg"

            try {
                Bitmap bitmap = encodeAsBitmap(text, dimension);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                GeneratedBarcode generatedBarcodeContextResult = new GeneratedBarcode(baos.toByteArray());
                sendContextEvent(requestId, generatedBarcodeContextResult, Integer.MAX_VALUE);

            } catch (Exception e) {
                e.printStackTrace();
                sendContextRequestError(requestId, "failed to create Barcode: " + e.getMessage(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
            }

        }
    }

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    public Bitmap encodeAsBitmap(String contents, int dimension) throws WriterException {

        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contents);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = writer.encode(contents, BarcodeFormat.QR_CODE, dimension, dimension, hints);
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) { return "UTF-8"; }
        }
        return null;
    }

    public void scanToInteractFeature() {
        Log.i(TAG, "scanToInteractFeature!");
        openView(new CameraTestActivity(getSecuredContext(), this, getSessionId(),
                SCAN_TO_INTERACT_FEATURE));
    }
}