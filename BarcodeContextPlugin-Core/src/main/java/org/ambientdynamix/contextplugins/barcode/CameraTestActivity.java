package org.ambientdynamix.contextplugins.barcode;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import net.sourceforge.zbar.*;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.UUID;

public class CameraTestActivity implements IPluginView {
    private final String TAG = this.getClass().getSimpleName();
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private ActivityController controller;
    private ImageScanner scanner;

    static {
        System.loadLibrary("iconv");
    }

    private boolean previewing = false;
    private Context context;
    private ContextPluginRuntime runtime;
    private UUID requestId;
    private String contextType;
    private boolean sentResult = false;

    public CameraTestActivity(Context context, ContextPluginRuntime runtime, UUID requestId, String contextType) {
        this.context = context;
        this.runtime = runtime;
        this.requestId = requestId;
        this.contextType = contextType;
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };
    PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            final Parameters parameters = camera.getParameters();
            Size size = parameters.getPreviewSize();
            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);
            int result = scanner.scanImage(barcode);
            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();
                if (result != 0) {
                    previewing = false;
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();
                    SymbolSet syms = scanner.getResults();
                    for (Symbol sym : syms) {
                        Log.v(TAG, "barcode result " + sym.getData());
                        if (contextType
                                .equals(BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE)) {
                            // Try to get symbol type string using reflection
                            String type = null;
                            try {
                                for (Field f : Symbol.class.getFields()) {
                                    if (f.getType().equals(int.class)) {
                                        if (f.getInt(Symbol.class) == sym.getType()) {
                                            //Log.i(getClass().getSimpleName(), "Barcode Format: " + f.getName());
                                            type = f.getName();
                                            break;
                                        }
                                    }
                                }
                            } catch (IllegalAccessException e) {
                                Log.w(TAG, "Error getting symbol type string from barcode format: " + e);
                            }
                            // If we didn't get the type, send its int value
                            if (type == null)
                                type = Integer.toString(sym.getType());
                            runtime.sendContextEvent(requestId, new BarcodeContextInfo(type, sym.getData()));

                            sentResult = true;
                        }
                    }
                } else {
                    // Scan failed
                    if (contextType
                            .equals(BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE)) {
                        runtime.sendContextRequestError(requestId, "Barcode Scan Failure", ErrorCodes.INTERNAL_PLUG_IN_ERROR);
                        sentResult = true;
                    }
                }
                controller.closeActivity();
            }
        }
    };


    // Mimic continuous auto-focusing
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    @Override
    public void setActivityController(ActivityController activityController) {
        controller = activityController;
    }

    @Override
    public View getView() {
        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();
//         Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);
        LinearLayout parentLayout = new LinearLayout(context);
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        parentLayout.setOrientation(LinearLayout.VERTICAL);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        mPreview = new CameraPreview(context, mCamera, previewCb, autoFocusCB);
        frameLayout.addView(mPreview);
        mCamera.setPreviewCallback(previewCb);
        mCamera.startPreview();
        previewing = true;
        parentLayout.addView(frameLayout);
        return parentLayout;
    }

    @Override
    public void destroyView() {

        /* If we have a context request ongoing, send cancelled.
         */
        if (contextType
                .equals(BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE) && !sentResult) {
            runtime.sendContextRequestError(requestId, "Barcode Scan: REQUEST_CANCELLED", ErrorCodes.REQUEST_CANCELLED);
        }
        releaseCamera();
        controller.closeActivity();
    }

    @Override
    public void handleIntent(Intent intent) {
        // Not handled
    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}
