# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.barcode` plug-in for the [Dynamix Framework](http://ambientdynamix.org). It allows apps to scan bar codes from various sources, such as video streams, image files and raw intensity sensors. It supports EAN-13/UPC-A, UPC-E, EAN-8, Code 128, Code 93, Code 39, Codabar, Interleaved 2 of 5, QR Code and DataBar. 

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Dynamix version 2.1 and higher

# Context Support
* `org.ambientdynamix.contextplugins.barcode.scan` initiate a bar code scan.

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.barcode", "org.ambientdynamix.contextplugins.barcode.scan", callback, listener);
```
## Handle Events
Once context support is successfully added, apps can make context requests to initiate a barcode scan. On a successful scan, the plug-in fires a context event of the type `BarcodeContextInfo`, which contains the following data :  

1. `format` : format of scanned bar code as a String. 
2. `value` : data encoded in the bar code as a String. 

Apps can make use of the `getBarcodeFormat` and `getBarcodeValue` methods of the `BarcodeContextInfo` class to retrieve these values. 






